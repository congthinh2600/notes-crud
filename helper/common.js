const fs = require('fs');
var {annoucement} = require('../annoucement');
var {COLORS_RES} = require('../variable');

function readFile(fileName, end_conding){
    try {
        const object = JSON.parse(fs.readFileSync(fileName, end_conding));
        return object
    } catch(err) {
        // console.log(err.toString())
        return []
    }
}

function writeFile(fileName, data, end_conding){
    const jsonString = JSON.stringify(data)
    try{
        fs.writeFileSync(fileName, jsonString, end_conding)
        return true;
    }catch(err){
        throw err.toString();
    }
}

function pipeData(data){
    let transformData = '';
    data.forEach((element, index, arr) => {
        transformData = transformData.concat((index+1).toString(), '. Title: ', element.title, ' - Body: ', element.body);
        if(index !== arr.length - 1){
            transformData = transformData.concat('\n');
        }
    });
    return transformData;
}

function pipeReadItem(data){
    return annoucement(data.title, COLORS_RES.INFO).toString() + '\n' + data.body;
}

module.exports = {
    readFile,
    writeFile,
    pipeData,
    pipeReadItem
}