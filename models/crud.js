var {annoucement} = require('../annoucement');
var {COLORS_RES, FILE_NAME, END_CODING} = require('../variable');
var {readFile, writeFile} = require('../helper/common')

function getAll(){
    return readFile(FILE_NAME,END_CODING);
}

function insertRecord(info){
    let result = readFile(FILE_NAME,END_CODING);
    result.push(info)
    return writeFile(FILE_NAME, result, END_CODING)
}

function updateRecord(data, index){
    let notes = readFile(FILE_NAME,END_CODING);
    Object.assign(notes[index], data);
    return writeFile(FILE_NAME, notes, END_CODING)
}

function readRecord(title){
    return findRecord(title);
}

function deleteRecord(title){
    let result = readFile(FILE_NAME,END_CODING);
    result = result.filter((item) => {
        return item.title !== title;
    })
    return writeFile(FILE_NAME, result, END_CODING)
}

function findRecord(title){
    let file = readFile(FILE_NAME,END_CODING);
    let result = {}
    file.forEach(element => {
        if(element.title === title) {result = element};
    });
    return result
}

function indexOfRecord(title){
    let file = readFile(FILE_NAME,END_CODING);
    let result = -1;
    file.forEach((element, index) => {
        if(element.title === title) {result = index};
    });
    return result
}

module.exports = {
    getAll,
    insertRecord,
    updateRecord,
    readRecord,
    deleteRecord,
    findRecord,
    indexOfRecord
}