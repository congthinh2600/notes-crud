var { list, insert, getOne, remove, update} = require('./controller/controller');
const argv = require('yargs')
var {validateCmd, validateFlag} = require('./helper/validator');

function cmdCheck(argv, actionType, action){
  if(!validateCmd(argv._)){
    return;
  }else{
    if(actionType === 'read' || actionType === 'remove'){
      if(!validateFlag('title', argv.title)){
        return
      }
      action(argv.title);
      return;
    }
    if(!validateFlag('title', argv.title) || !validateFlag('body', argv.body)){
      return;
    }
    let obj = {
      title: argv.title,
      body: argv.body
    }
    action(obj);
  }
}

argv.command({
  command: 'add',
  desc: 'add new note',
  handler: (argv)=>{
    cmdCheck(argv,'add', insert)
  }
})
.command({
  command: 'remove',
  desc: 'remove a note',
  handler: (argv)=>{
    cmdCheck(argv, 'remove', remove);
  }
})
.command({
  command: 'read',
  desc: 'read a note',
  handler: (argv)=>{
    cmdCheck(argv, 'read', getOne);
  }
})
.command({
  command: 'update',
  desc: 'update note',
  handler: (argv)=>{
    cmdCheck(argv, 'update', update)
  }
})
.command({
  command: 'list',
  aliases: ['ls'],
  desc: 'show list of notes',
  builder: (yargs) => {
    
  },
  handler: (argv) => {
    if(!validateCmd(argv._)){
      return;
    }   
    list();    
  }
})
.demandCommand()
.help()
.wrap(72)
.argv

module.exports = {
    argv
}