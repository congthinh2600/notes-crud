const colors = require('colors/safe');

colors.setTheme({
    success: 'bgGreen',
    info: 'bgBrightWhite',
    error: 'bgBrightRed'
  });

function annoucement(message, status){
    switch (status) {
        case "SUCCESS":
            return colors.success(colors.black(message));
            break;
        case "ERROR":
            return colors.error(colors.black(message));
            break
        default:
            return colors.info(message);
            break;
    }
}

module.exports = {
    annoucement
}