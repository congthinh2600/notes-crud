const COLORS_RES = {
    "SUCCESS":"SUCCESS",
    "ERROR": "ERROR",
    "INFO":"INFO"
}

const FILE_NAME = './models/store.json';
const END_CODING ='utf-8';
const CMD_LIST  = ['list', 'add', 'update', 'remove', 'read'];

module.exports = {
    COLORS_RES,
    FILE_NAME,
    END_CODING,
    CMD_LIST
}